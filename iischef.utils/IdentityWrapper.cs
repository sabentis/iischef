﻿using System;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Security.Principal;
using Microsoft.SqlServer.Management.Assessment.Configuration;

namespace iischef.utils
{
    /// <summary>
    /// 
    /// </summary>
    public class IdentityWrapper
    {
        /// <summary>
        /// 
        /// </summary>
        protected SecurityIdentifier SidValue;

        /// <summary>
        /// 
        /// </summary>
        protected string NameValue;

        /// <summary>
        /// 
        /// </summary>
        protected string SamAccountNameValue;

        /// <summary>
        /// 
        /// </summary>
        protected string PasswordValue;

        /// <summary>
        /// 
        /// </summary>
        protected string DisplayNameValue;

        /// <summary>
        /// 
        /// </summary>
        public ContextType? ContextTypeValue;

        /// <summary>
        /// 
        /// </summary>
        protected AccountManagementPrincipalContext PrincipalContextValue;

        /// <summary>
        /// 
        /// </summary>
        public string Name => this.NameValue;

        /// <summary>
        /// 
        /// </summary>
        public string SamAccountName => this.SamAccountNameValue;

        /// <summary>
        /// 
        /// </summary>
        public string Password => this.PasswordValue;

        /// <summary>
        /// 
        /// </summary>
        public AccountManagementPrincipalContext PrincipalContext => this.PrincipalContextValue;

        /// <summary>
        /// 
        /// </summary>
        public SecurityIdentifier Sid => this.SidValue;

        /// <summary>
        /// 
        /// </summary>
        public string DisplayName => this.DisplayNameValue;

        /// <summary>
        /// 
        /// </summary>
        public ContextType? ContextType => this.ContextTypeValue;

        /// <summary>
        /// 
        /// </summary>
        protected IdentityWrapper()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="samaccountname"></param>
        /// <param name="passwordValue"></param>
        /// <param name="displayName"></param>
        /// <param name="principalContextValue"></param>
        /// <returns></returns>
        public static IdentityWrapper FromUserInfo(
            string name,
            string samaccountname,
            string passwordValue,
            string displayName,
            AccountManagementPrincipalContext principalContextValue)
        {
            var wrapper = new IdentityWrapper();

            wrapper.NameValue = name;
            wrapper.SamAccountNameValue = samaccountname;
            wrapper.PasswordValue = passwordValue;
            wrapper.PrincipalContextValue = principalContextValue;
            wrapper.DisplayNameValue = displayName;

            if (principalContextValue != null)
            {
                wrapper.ContextTypeValue = System.DirectoryServices.AccountManagement.ContextType.Domain;
            }
            else
            {
                wrapper.ContextTypeValue = System.DirectoryServices.AccountManagement.ContextType.Machine;
            }

            return wrapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="machineName"></param>
        /// <returns></returns>
        public static IdentityWrapper FromMachineName(string machineName)
        {
            if (!machineName.EndsWith("$"))
            {
                machineName = machineName + "$";
            }

            var wrapper = new IdentityWrapper();

            wrapper.NameValue = machineName;

            return wrapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static IdentityWrapper FromUnkown(string name)
        {
            var wrapper = new IdentityWrapper();

            // Backwards compatibility fix
            if (name.StartsWith("sid:"))
            {
                name = name.Replace("sid:", string.Empty).Trim();
            }

            string domainName = null;

            if (name.Contains("@"))
            {
                domainName = name.Split("@".ToCharArray()).Last();
                name = name.Split("@".ToCharArray()).First();
            }

            if (name.Contains("\\"))
            {
                domainName = name.Split("\\".ToCharArray()).First();
                name = name.Split("\\".ToCharArray()).Last();
            }

            if (UtilsSystem.IsValidSid(name))
            {
                // Assume we are using the user principal name, we cannot determine the context here...
                wrapper.SidValue = new SecurityIdentifier(name);
            }
            else
            {
                wrapper.NameValue = name;
            }

            if (name.StartsWith("#"))
            {
                throw new Exception("Unsupported prefix # used in identity definition.");

                // Assume we are using the user principal name, we cannot determine the context here...
                // this.UserPrincipalName = name.Replace("#", string.Empty);
                // this.SamAccountName = name.Replace("#", string.Empty);
            }

            if (!string.IsNullOrWhiteSpace(domainName))
            {
                wrapper.ContextTypeValue = (Environment.MachineName == domainName || domainName.Equals("localhost", StringComparison.CurrentCultureIgnoreCase))
                    ? System.DirectoryServices.AccountManagement.ContextType.Machine
                    : System.DirectoryServices.AccountManagement.ContextType.Domain;
            }

            if (!string.IsNullOrWhiteSpace(domainName))
            {
                wrapper.PrincipalContextValue = new AccountManagementPrincipalContext();
                wrapper.PrincipalContextValue.DomainName = domainName;
            }

            return wrapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preWindows200"></param>
        /// <returns></returns>
        public string FormatUserNameForPrincipal(bool preWindows200 = false)
        {
            if (this.PrincipalContextValue != null)
            {
                return this.DoFormatUserNameForPrincipal(preWindows200);
            }
            else
            {
                return Environment.MachineName + "\\" + this.SamAccountName;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preWindows2000"></param>
        /// <returns></returns>
        protected string DoFormatUserNameForPrincipal(bool preWindows2000)
        {
            // If this is a domain principal, use a domain name.
            if (this.PrincipalContextValue.ContextType == nameof(System.DirectoryServices.AccountManagement.ContextType.Domain))
            {
                string domainName = this.PrincipalContextValue.DomainName;

                if (string.IsNullOrWhiteSpace(domainName))
                {
                    domainName = Domain.GetComputerDomain().Name;

                    // For compatiblity with SQL Server and legacy apps, use main DC.
                    // As sometimes domain name will return DC1.DC2.DC3
                    // and we only need DC1
                    domainName = domainName.Split(".".ToCharArray()).First().ToUpper();
                }

                if (preWindows2000)
                {
                    string mainDomain = domainName.Split(".".ToCharArray()).First().ToUpper();
                    return $"{mainDomain}\\{this.SamAccountNameValue}";
                }
                else
                {
                    return $"{this.NameValue}@{domainName}";
                }
            }

            return this.SamAccountNameValue;
        }
    }
}
