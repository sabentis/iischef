﻿using System;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace iischef.utils
{
    public delegate void Eventhandler(object sender, string line, bool error);

    public class ConsoleCommand : IDisposable
    {
        public event Eventhandler LineAdded;

        private Process Process;

        private StreamReaderLineInput reader;

        public ConsoleCommand()
        {
            StreamReaderLineInput readerStandardOutput;
            StreamReaderLineInput readerErrorOutput;

            this.Process = new Process();

            // Configuramos el proceso.
            this.Process.StartInfo.FileName = "cmd.exe";
            this.Process.StartInfo.CreateNoWindow = true;
            this.Process.StartInfo.Arguments = "/k";
            this.Process.StartInfo.RedirectStandardInput = true;
            this.Process.StartInfo.RedirectStandardOutput = true;
            this.Process.StartInfo.StandardOutputEncoding = Encoding.UTF8;
            this.Process.StartInfo.StandardErrorEncoding = Encoding.UTF8;
            this.Process.StartInfo.RedirectStandardError = true;
            this.Process.StartInfo.UseShellExecute = false;
            this.Process.StartInfo.ErrorDialog = false;
            this.Process.EnableRaisingEvents = true;
            this.Process.Start();

            // El problema de leer esto en dos threads separados
            // es que se pueden solapar mensajes que deberían ir juntos!!
            // lo que nos obliga a buscar periodos de inactividad para
            // mostrar los errores.
            this.reader = new StreamReaderLineInput((a, b) => this.AddLine(b, a == "error"));

            this.reader.AddReader("output", this.Process.StandardOutput);
            this.reader.AddReader("error", this.Process.StandardError);

            this.reader.StartReading();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="argument"></param>
        /// <returns></returns>
        public static string EscapeForArgument(string argument)
        {
            argument = argument.Replace("\"", "\"\"");
            return "\"" + argument + "\"";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public int RunCommandAndWait(string command)
        {
            // https://ss64.com/nt/errorlevel.html
            command += Environment.NewLine + $"echo finished:%ERRORLEVEL%";

            bool hasError = false;
            bool finished = false;

            int exitCode = 0;

            this.LineAdded += (object sender, string line, bool error) =>
            {
                var cleanLine = line
                    .Replace(((char)32).ToString(), string.Empty)
                    .Replace("\r", string.Empty)
                    .Replace("\n", string.Empty);

                // If this is a clear-console line, sleep a little
                if (cleanLine == string.Empty)
                {
                    Thread.Sleep(150);
                }

                if (error)
                {
                    Console.Error.Write(line);
                }
                else
                {
                    Console.Out.Write(line);
                }

                var match = Regex.Match(cleanLine, "^finished:(.*)");

                if (match.Success)
                {
                    if (match.Groups.Count > 1)
                    {
                        int.TryParse(match.Groups[1].Value, out exitCode);
                    }

                    finished = true;
                    return;
                }

                hasError = hasError ? hasError : error;
            };

            this.RunCommand(command);

            while (!finished)
            {
                Thread.Sleep(300);
            }

            if (hasError)
            {
                throw new Exception("Command error");
            }

            return exitCode;
        }

        public void RunCommand(string cmd)
        {
            this._RunCommand(cmd);
        }

        private void _RunCommand(string cmd)
        {
            this._SendInput(cmd);
        }

        /// <summary>
        /// El output de consola se va añadiendo
        /// caracter a caracter, pero luego se hace un post-procesado.
        /// </summary>
        private void AddLine(string output, bool error)
        {
            lock (string.Intern("reading-buffer"))
            {
                this.LineAdded?.Invoke(this, output, error);
            }
        }

        private void _SendInput(string input)
        {
            this.Process.StandardInput.Flush();
            this.Process.StandardInput.WriteLineAsync(input);
        }

        public void Dispose()
        {
            // Close the console process, if still opened.
            this.reader.StopReading();

            // Make sure we reset the return code..
            this.Process.StandardInput.WriteLine("exit 0");
            if (!this.Process.WaitForExit(1500))
            {
                this.Process.Kill();
            }

            this.Process.Close();
            this.Process.Dispose();
        }
    }
}
