﻿using System;
using System.IO;
using System.Windows;

namespace iischef.utils
{
    public static class CompressionUtils
    {
        /// <summary>
        /// Extract file using 7ZIP
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="targetDirectory"></param>
        public static void ExtractWith7z(string sourcePath, string targetDirectory)
        {
            // https://superuser.com/questions/519114/how-to-write-error-status-for-command-line-7-zip-in-variable-or-instead-in-te

            ////0 = No error.
            ////1 = Warning(Non fatal error(s)).For example, one or more files were locked by some other application, so they were not compressed.
            ////2 = Fatal error.
            ////7 = Command line error.
            ////8 = Not enough memory for operation.
            ////255 = User stopped the process.

            var command = $"7z x {ConsoleCommand.EscapeForArgument(sourcePath)} -aoa -o{ConsoleCommand.EscapeForArgument(targetDirectory)} -y";

            using (var console = new ConsoleCommand())
            {
                int exitCode = console.RunCommandAndWait(command);

                if (exitCode != 0)
                {
                    throw new InvalidDataException("Error extracting file: " + sourcePath);
                }
            }
        }

        /// <summary>
        /// Extract file using 7ZIP
        /// </summary>
        /// <param name="sourceDirectory"></param>
        /// <param name="targetFile"></param>
        public static void CreateWith7Z(string sourceDirectory, string targetFile)
        {
            if (!Directory.Exists(sourceDirectory))
            {
                throw new Exception("Source directory does not exist.");
            }

            // https://superuser.com/questions/519114/how-to-write-error-status-for-command-line-7-zip-in-variable-or-instead-in-te

            ////0 = No error.
            ////1 = Warning(Non fatal error(s)).For example, one or more files were locked by some other application, so they were not compressed.
            ////2 = Fatal error.
            ////7 = Command line error.
            ////8 = Not enough memory for operation.
            ////255 = User stopped the process.

            var command = $"7z a {ConsoleCommand.EscapeForArgument(targetFile)} {ConsoleCommand.EscapeForArgument(sourceDirectory + "\\*")}";

            using (var console = new ConsoleCommand())
            {
                int exitCode = console.RunCommandAndWait(command);

                if (exitCode != 0)
                {
                    throw new InvalidDataException("Error compressing directory: " + sourceDirectory);
                }
            }
        }
    }
}
