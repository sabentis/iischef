﻿using System;

namespace iischef.utils.AppVeyor
{
    public class AppVeyorArtifactNotFoundException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public AppVeyorArtifactNotFoundException(string message, Exception inner = null) : base(message, inner)
        {
        }
    }
}
