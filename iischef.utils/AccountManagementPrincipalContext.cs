﻿namespace iischef.utils
{
    /// <summary>
    /// The information used to handle user creation and permission management for applications
    /// </summary>
    public class AccountManagementPrincipalContext
    {
        public string ContextType { get; set; }

        public string ContextName { get; set; }

        public string Container { get; set; }

        public string ContextOptions { get; set; }

        public string ContextUsername { get; set; }

        public string ContextPassword { get; set; }

        /// <summary>
        /// Optional domain name, will be automatically resolved to the current machine's domain name if empty
        /// </summary>
        public string DomainName { get; set; }
    }
}
