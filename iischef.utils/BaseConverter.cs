﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iischef.utils
{
    public class BaseConverter
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly char[] BaseChars;

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<char, int> CharValues;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseChars"></param>
        public BaseConverter(string baseChars =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
        {
            this.BaseChars = baseChars.ToCharArray();

            this.CharValues = this.BaseChars
                .Select((c, i) => new { Char = c, Index = i })
                .ToDictionary(c => c.Char, c => c.Index);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string LongToBase(long value)
        {
            long targetBase = this.BaseChars.Length;

            // Determine exact number of characters to use.
            char[] buffer = new char[Math.Max(
                (int)Math.Ceiling(Math.Log(value + 1, targetBase)), 1)];

            var i = buffer.Length;

            do
            {
                buffer[--i] = this.BaseChars[value % targetBase];
                value = value / targetBase;
            }
            while (value > 0);

            return new string(buffer, i, buffer.Length - i);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public long BaseToLong(string number)
        {
            char[] chrs = number.ToCharArray();
            int m = chrs.Length - 1;
            int n = this.BaseChars.Length, x;
            long result = 0;

            for (int i = 0; i < chrs.Length; i++)
            {
                x = this.CharValues[chrs[i]];
                result += x * (long)Math.Pow(n, m--);
            }

            return result;
        }
    }
}
