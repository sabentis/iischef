﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace iischef.core.Php
{
    /// <summary>
    /// Represents a PHP environment
    /// </summary>
    public class PhpEnvironment
    {
        /// <summary>
        /// The environment id.
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// List of deployment operations for the environment
        /// </summary>
        public Dictionary<string, JToken> runtime { get; set; }

        /// <summary>
        /// Environment variables to use for all the PHP runtimes.
        /// </summary>
        public Dictionary<string, string> environmentVariables { get; set; }

        /// <summary>
        /// FastCGI process activity timeout.
        /// </summary>
        public int? activityTimeout { get; set; }

        /// <summary>
        /// FastCGI request timeout.
        /// </summary>
        public int? requestTimeout { get; set; }

        /// <summary>
        /// Max number of PHP processes.
        /// </summary>
        public int? maxInstances { get; set; }

        /// <summary>
        /// Maximum number of requests that can be served by a PHP process
        /// before being recycled.
        /// </summary>
        public int? instanceMaxRequests { get; set; }

        /// <summary>
        /// The response buffer limit on the IIS side. If you wan to to use dynamic content
        /// compression you need the response to be buffered. On the other side, the use
        /// of technologies such as response streaming (BigPipe in Drupal) require
        /// this to be set to 0, see:
        /// https://www.drupal.org/docs/8/core/modules/big-pipe/bigpipe-environment-requirements
        /// https://support.microsoft.com/en-us/help/2321250/asp-response-buffer-settings-not-honored-in-iis-7-0-and-iis-7-5
        /// </summary>
        public int? responseBufferLimit { get; set; }
    }
}
