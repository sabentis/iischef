﻿namespace iischef.core
{
    /// <summary>
    /// 
    /// </summary>
    public enum UndeployMode
    {
        /// <summary>
        /// Non persistent services are removed
        /// </summary>
        Undeploy = 0,

        /// <summary>
        /// Non persistent and persistent services are removed
        /// </summary>
        UninstallAndCleanup = 1,

        /// <summary>
        /// Uninstall
        /// </summary>
        Uninstall = 2
    }
}
