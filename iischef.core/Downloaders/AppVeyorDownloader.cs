﻿using iischef.core.SystemConfiguration;
using iischef.logger;
using iischef.utils;
using iischef.utils.AppVeyor;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace iischef.core.Downloaders
{
    /// <summary>
    /// Download artifacts from AppVeyor
    /// </summary>
    public class AppVeyorDownloader : IDownloaderInterface
    {
        protected AppVeyorDownloaderSettings Settings;

        protected Client Client;

        protected ILoggerInterface Logger;

        protected EnvironmentSettings GlobalSettings;

        protected string ApplicationId;

        protected string TempDir;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="globalSettings"></param>
        /// <param name="logger"></param>
        /// <param name="tempDir">Directory to use for temporary storage.</param>
        /// <param name="applicationId">Application ID, this will be used to customize temp storage paths per application.</param>
        public AppVeyorDownloader(
            AppVeyorDownloaderSettings settings,
            EnvironmentSettings globalSettings,
            ILoggerInterface logger,
            string tempDir,
            string applicationId)
        {
            this.ApplicationId = applicationId;
            this.Settings = settings;
            this.Logger = logger;
            this.GlobalSettings = globalSettings;
            this.Client = new Client(settings.apitoken, "https://ci.appveyor.com", logger, tempDir);
            this.TempDir = tempDir;
        }

        /// <inheritdoc cref="IDownloaderInterface"/>
        public string GetNextId(string buildId = null)
        {
            // Bring all successful jobs
            var lastBuilds = this.Client.FindLastSuccessfulBuilds(
                this.Settings.username,
                this.Settings.project,
                this.Settings.branch,
                buildVersionRequested: buildId,
                exp: this.Settings.publish_regex_filter,
                maxResults: 2);

            if (!lastBuilds.Any())
            {
                throw new Exception(
                    $"No suitable successful build (buildId={buildId}) found for project {this.Settings.project} on branch {this.Settings.branch}");
            }

            // The build ID uniquely identifies this build.
            return lastBuilds.First().version;
        }

        public string RemoveInvalidChars(string filename)
        {
            return string.Concat(filename.Split(Path.GetInvalidFileNameChars()));
        }

        /// <inheritdoc cref="IDownloaderInterface"/>
        public Artifact PullFromId(string version, string preferredLocalArtifactPath)
        {
            // AppVeyor artifacts expire after 3 months, to eliminate dependency
            // on AppVeyor artifact retention we will store a full local copy of the
            // obtained artifact in a cache.
            string artifactCache =
                Path.Combine(
                    this.TempDir,
                    "_appveyor_artifacts",
                    this.ApplicationId,
                    this.RemoveInvalidChars(this.Settings.project));

            UtilsSystem.EnsureDirectoryExists(artifactCache, true);

            // Downloader details can actually vary with several arguments, just
            // hash the whole object...
            var cacheVersion = version + "_" + this.GetSignature(this.Settings.SerializeToJson(), string.Empty);

            // No sabemos la ubicación final del item de cache porque no conocemos la rama (los artefactos
            // se ordenan en directorios por rama
            var cacheFile = Directory
                .EnumerateFiles(artifactCache, $"{cacheVersion}.json", SearchOption.AllDirectories)
                .FirstOrDefault(i => Path.GetFileName(i) == $"{cacheVersion}.json");

            if (!string.IsNullOrWhiteSpace(cacheFile))
            {
                var cachedArtifact = this.ArtifactFromCache(cacheFile, preferredLocalArtifactPath);

                if (cachedArtifact != null)
                {
                    this.Logger.LogInfo(true, "Appveyor artifact restored from local cache {0}", cacheFile);
                    return cachedArtifact;
                }
            }

            Artifact artifact = new Artifact
            {
                id = version,
                localPath = preferredLocalArtifactPath,
                isRemote = true
            };

            // Use artifact temp path, or local system temporary directory.
            if (Directory.Exists(artifact.localPath))
            {
                UtilsSystem.DeleteDirectory(artifact.localPath, this.Logger);
            }

            // Use the build version to pull the build information.
            Build build = this.Client.GetBuildFromVersion(version, this.Settings.username, this.Settings.project);

            // Make sure that the builds matches the current active branch, otherwise throw an exception
            if (!build.branch.Equals(this.Settings.branch, StringComparison.CurrentCultureIgnoreCase))
            {
                throw new Exception($"Requested version '{version}' with branch '{build.branch}' does not belong to active settings branch '{this.Settings.branch}'");
            }

            this.Client.DownloadSingleArtifactFromBuild(this.ApplicationId, build, this.Settings.artifact_regex, artifact.localPath, this.Logger, out var persitentArtifactSourceCachePath);

            artifact.artifactSettings = new ArtifactSettings();
            artifact.artifactSettings.PopulateFromSettingsFile(artifact.localPath, this.Logger);

            if (string.IsNullOrWhiteSpace(artifact.artifactSettings.branch))
            {
                artifact.artifactSettings.branch = Convert.ToString(build.branch);
            }

            if (string.IsNullOrWhiteSpace(artifact.artifactSettings.commit_sha))
            {
                artifact.artifactSettings.commit_sha = Convert.ToString(build.commitId);
            }

            if (string.IsNullOrWhiteSpace(artifact.artifactSettings.version))
            {
                artifact.artifactSettings.version = build.version;
            }

            if (string.IsNullOrWhiteSpace(artifact.artifactSettings.commit_message))
            {
                artifact.artifactSettings.commit_message = Convert.ToString(build.message);
            }

            var branchDir = Path.Combine(artifactCache, artifact.artifactSettings.branch);
            UtilsSystem.EnsureDirectoryExists(branchDir, true);

            cacheFile = Path.Combine(branchDir, cacheVersion + ".json");
            this.ArtifactToCache(cacheFile, artifact, persitentArtifactSourceCachePath);

            this.Logger.LogInfo(true, "Appveyor artifact saved to local cache {0}", cacheFile);

            // Keep only a limited number of artifacts per branch (latest N)
            var filesToRemove = Directory.EnumerateFiles(branchDir, "*.*")
                .Where((i) => i.EndsWith(".json"))
                .OrderByDescending((i) => File.GetCreationTimeUtc(i))
                .Skip(25)
                .ToList();

            foreach (var file in filesToRemove)
            {
                UtilsSystem.DeleteFile(file, this.Logger);
            }

            return artifact;
        }

        /// <summary>
        /// Get a compact string signature hash for the provided string, using a salt.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        protected string GetSignature(string source, string salt)
        {
            BaseConverter bc = new BaseConverter();
            byte[] encoded = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(source + salt));
            var value = BitConverter.ToUInt32(encoded, 0);
            var signature = bc.LongToBase((long)value);
            return signature;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheInfoFile"></param>
        /// <param name="artifact"></param>
        /// <param name="persistentArtifactCacheFile"></param>
        protected void ArtifactToCache(string cacheInfoFile, Artifact artifact, string persistentArtifactCacheFile)
        {
            CacheInfo info = new CacheInfo();
            info.Artifact = artifact.JsonClone();
            info.Artifact.localPath = null;
            info.PersistentArtifactLocation = persistentArtifactCacheFile;

            this.Logger.LogInfo(true, "Generating artifact file info at: " + cacheInfoFile);
            File.WriteAllText(cacheInfoFile, info.SerializeToJson());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheInfoFile"></param>
        /// <param name="preferredLocalArtifactPath"></param>
        /// <returns></returns>
        protected Artifact ArtifactFromCache(string cacheInfoFile, string preferredLocalArtifactPath)
        {
            try
            {
                CacheInfo info = JsonConvert.DeserializeObject<CacheInfo>(File.ReadAllText(cacheInfoFile));

                if (!File.Exists(info.PersistentArtifactLocation))
                {
                    return null;
                }

                this.Logger.LogInfo(true, "Extracting cached artifact from file: " + info.PersistentArtifactLocation);
                CompressionUtils.ExtractWith7z(info.PersistentArtifactLocation, preferredLocalArtifactPath);

                Artifact artifact = info.Artifact;
                artifact.localPath = preferredLocalArtifactPath;
                return artifact;
            }
            catch (Exception e)
            {
                this.Logger.LogWarning(false, e.Message);
            }

            return null;
        }

        public Client GetClient()
        {
            return this.Client;
        }

        public AppVeyorDownloaderSettings GetSettings()
        {
            return this.Settings;
        }

        /// <summary>
        /// 
        /// </summary>
        public class CacheInfo
        {
            /// <summary>
            /// 
            /// </summary>
            public Artifact Artifact { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string PersistentArtifactLocation { get; set; }
        }
    }
}
