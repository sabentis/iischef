﻿using Microsoft.Web.Administration;

namespace iischef.core.Configuration
{
    /// <summary>
    /// These are specific configuration limits to be configured
    /// on the server itself, that will have an effect/limit on the different
    /// deployer settings. These settings are related to application stability
    /// and resource contention.
    /// </summary>
    public class ApplicationLimits
    {
        /// <summary>
        /// Populate limit defaults if they are missing
        /// </summary>
        public static void MergeMissingLimits(ApplicationLimits target, ApplicationLimits source)
        {
            if (source == null)
            {
                return;
            }

            target.IisPoolMaxPrivateMemoryLimitKb = target.IisPoolMaxPrivateMemoryLimitKb ?? source.IisPoolMaxPrivateMemoryLimitKb;
            target.IisPoolCpuLimitAction = target.IisPoolCpuLimitAction ?? source.IisPoolCpuLimitAction;
            target.IisPoolMaxCpuLimitPercent = target.IisPoolMaxCpuLimitPercent ?? source.IisPoolMaxCpuLimitPercent;
            target.IisPoolStartupModeAllowAlwaysRunning = target.IisPoolStartupModeAllowAlwaysRunning ?? source.IisPoolStartupModeAllowAlwaysRunning;
            target.IisVirtualDirectoryAllowPreloadEnabled = target.IisVirtualDirectoryAllowPreloadEnabled ?? source.IisVirtualDirectoryAllowPreloadEnabled;
            target.IisPoolIdleTimeoutAction = target.IisPoolIdleTimeoutAction ?? source.IisPoolIdleTimeoutAction;
            target.FastCgiActivityTimeout = target.FastCgiActivityTimeout ?? source.FastCgiActivityTimeout;
            target.FastCgiInstanceMaxRequests = target.FastCgiInstanceMaxRequests ?? source.FastCgiInstanceMaxRequests;
            target.FastCgiMaxInstances = target.FastCgiMaxInstances ?? source.FastCgiMaxInstances;
            target.FastCgiRequestTimeout = target.FastCgiRequestTimeout ?? source.FastCgiRequestTimeout;
            target.RapidFailProtection = target.RapidFailProtection ?? source.RapidFailProtection;
            target.RapidFailProtectionIntervalMinutes = target.RapidFailProtectionIntervalMinutes ?? source.RapidFailProtectionIntervalMinutes;
            target.RapidFailProtectionMaxCrashes = target.RapidFailProtectionMaxCrashes ?? source.RapidFailProtectionMaxCrashes;
        }

        /// <summary>
        /// Populate limit defaults if they are missing
        /// </summary>
        public static ApplicationLimits GetDefaultApplicationLimits()
        {
            ApplicationLimits limits = new ApplicationLimits();
            limits.IisPoolMaxPrivateMemoryLimitKb = (int)4 * 1024 * 1024;
            limits.IisPoolCpuLimitAction = ProcessorAction.ThrottleUnderLoad.ToString();
            limits.IisPoolMaxCpuLimitPercent = 60;
            limits.IisPoolStartupModeAllowAlwaysRunning = true;
            limits.IisVirtualDirectoryAllowPreloadEnabled = true;
            limits.IisPoolIdleTimeoutAction = IdleTimeoutAction.Terminate.ToString();
            return limits;
        }

        /// <summary>
        /// 
        /// </summary>
        public int? FastCgiMaxInstances { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? FastCgiInstanceMaxRequests { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? FastCgiActivityTimeout { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? FastCgiRequestTimeout { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? IisPoolMaxCpuLimitPercent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IisPoolCpuLimitAction { get; set; }

        /// <summary>
        /// Private Memory Limit (Kb)
        /// </summary>
        public int? IisPoolMaxPrivateMemoryLimitKb { get; set; }

        /// <summary>
        /// Pool startup mode + PreloadEnabled can be disabled for high density environments
        /// where we do not want sites to be fully loaded and sitting idle
        /// </summary>
        public bool? IisPoolStartupModeAllowAlwaysRunning { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? IisVirtualDirectoryAllowPreloadEnabled { get; set; }

        /// <summary>
        /// Action to take when pool is idle
        /// </summary>
        public string IisPoolIdleTimeoutAction { get; set; }

        /// <summary>
        /// Rapid-Fail Protection Enabled
        /// </summary>
        public bool? RapidFailProtection { get; set; }

        /// <summary>
        /// Failure interval (minutes)
        /// </summary>
        public long? RapidFailProtectionIntervalMinutes { get; set; }

        /// <summary>
        /// Maximum failures
        /// </summary>
        public long? RapidFailProtectionMaxCrashes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? ShutdownTimeLimitSeconds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? StartupTimeLimitSeconds { get; set; }
    }
}
