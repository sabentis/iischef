﻿using iischef.logger;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iischef.core.Configuration
{
    /// <summary>
    /// Deployment/Maintenance windows for applications
    /// </summary>
    public class DeploymentWindow
    {
        public static Dictionary<string, DeploymentWindow> ParseDeploymentWindows(JToken source, ILoggerInterface logger)
        {
            if (source == null)
            {
                return null;
            }

            try
            {
                var result = JsonConvert.DeserializeObject<Dictionary<string, DeploymentWindow>>(source.ToString());

                // Validate the timezone names
                foreach (var t in result)
                {
                    try
                    {
                        TimeZoneInfo.FindSystemTimeZoneById(t.Value.timezone);
                    }
                    catch (Exception e)
                    {
                        var timezoneIds = TimeZoneInfo.GetSystemTimeZones().Select((i) => i.Id);

                        logger.LogError("Invalid timezone:" + t.Value.timezone + " use one of " + string.Join(", ", timezoneIds));
                        return null;
                    }

                    try
                    {
                        TimeSpan.Parse(t.Value.start);
                    }
                    catch (Exception e)
                    {
                        logger.LogError("Could not parse start time for deployment window: " + t.Value.start);
                        return null;
                    }

                    try
                    {
                        TimeSpan.Parse(t.Value.end);
                    }
                    catch (Exception e)
                    {
                        logger.LogError("Could not parse end time for deployment window: " + t.Value.end);
                        return null;
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
                return null;
            }
        }

        /// <summary>
        /// The time zone for this deployment window
        /// </summary>
        public string timezone { get; set; }

        /// <summary>
        /// The start time
        /// </summary>
        public string start { get; set; }

        /// <summary>
        /// The end time
        /// </summary>
        public string end { get; set; }
    }
}
