﻿using System;
using System.Management.Automation;
using Amazon.Runtime.Internal.Util;
using iischef.logger;

namespace iischef.core
{
    public class Console : IDisposable
    {
        protected PowerShell ps;
        protected ILoggerInterface Logger;

        public Console(ILoggerInterface logger)
        {
            this.ps = PowerShell.Create();
            this.Logger = logger;

            this.ps.Streams.Progress.DataAdded += this.Progress_DataAdded;
            this.ps.Streams.Debug.DataAdded += this.Debug_DataAdded;
            this.ps.Streams.Error.DataAdded += this.Error_DataAdded;
            this.ps.Streams.Information.DataAdded += this.Information_DataAdded;
            this.ps.Streams.Warning.DataAdded += this.Warning_DataAdded;
        }

        private void Warning_DataAdded(object sender, DataAddedEventArgs e)
        {
            this.Logger.LogInfo(true, e.Index.ToString());
        }

        private void Information_DataAdded(object sender, DataAddedEventArgs e)
        {
            this.Logger.LogInfo(true, e.Index.ToString());
        }

        private void Error_DataAdded(object sender, DataAddedEventArgs e)
        {
            if (sender is PSDataCollection<ErrorRecord> errorRecords)
            {
                foreach (var errorRecord in errorRecords)
                {
                    this.Logger.LogException(errorRecord.Exception);
                }

                return;
            }

            this.Logger.LogInfo(true, e.Index.ToString());
        }

        private void Debug_DataAdded(object sender, DataAddedEventArgs e)
        {
            this.Logger.LogInfo(true, e.Index.ToString());
        }

        private void Progress_DataAdded(object sender, DataAddedEventArgs e)
        {
            this.Logger.LogInfo(true, e.Index.ToString());
        }

        /// <summary>
        /// Runs a script synchronously
        /// </summary>
        /// <param name="command"></param>
        public void RunCommand(string command)
        {
            this.Logger.LogInfo(false, "Running command: " + command);
            this.ps.AddScript(command);
            this.ps.Invoke();
        }

        public void Dispose()
        {
            if (this.ps != null)
            {
                this.ps.Dispose();
                this.ps = null;
            }
        }
    }
}
