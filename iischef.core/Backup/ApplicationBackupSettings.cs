﻿namespace iischef.core.Backup
{
    /// <summary>
    /// 
    /// </summary>
    public class ApplicationBackupSettings
    {
        /// <summary>
        /// Backup since date
        /// </summary>
        public string ChangedSince { get; set; }

        /// <summary>
        /// Target directory for the backup
        /// </summary>
        public string TargetDirectory { get; set; }
    }
}
