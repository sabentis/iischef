﻿namespace iischef.core.Backup
{
    /// <summary>
    /// Interface for deployers to support backup
    /// </summary>
    internal interface IDeployerBackupInterface
    {
        void Backup(ApplicationBackupSettings backupSettings);
    }
}
