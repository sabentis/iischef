﻿using iischef.utils;
using Newtonsoft.Json.Linq;
using System;

namespace iischef.core.Services
{
    internal class CouchbaseService : DeployerBase, IDeployerInterface
    {
        public void deploy()
        {
            var couchbaseSettings = this.DeployerSettings.castTo<CouchbaseServiceSettings>();

            // There is nothing really here. You simply request
            // an URI, username and password for a bucket. The application
            // is responsible for prefixing it's keys with something
            // unique...
            var couchbaseServer = this.GlobalSettings.GetDefaultCouchbaseServer();

            if (couchbaseServer == null && JToken.FromObject(true).ToString().Equals(this.DeployerSettings["optional"]?.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                this.Logger.LogWarning(false, "Application requested a couchbase, but no couchbase server was found. Skiping because service is marked as optional.");
                return;
            }

            if (couchbaseServer == null)
            {
                throw new Exception("Could not find couchbase service in the global configuration.");
            }

            this.Deployment.SetRuntimeSetting($"services.{couchbaseSettings.id}.uri", couchbaseServer.uri);
            this.Deployment.SetRuntimeSetting($"services.{couchbaseSettings.id}.bucket-name", couchbaseServer.bucketName);
            this.Deployment.SetRuntimeSetting($"services.{couchbaseSettings.id}.bucket-password", couchbaseServer.bucketPassword);
        }

        public void undeploy(UndeployMode mode)
        {
        }

        public void start()
        {
        }

        public void stop()
        {
        }

        public void shutdown()
        {
        }

        public void deploySettings(
            string jsonSettings,
            string jsonSettingsNested,
            RuntimeSettingsReplacer replacer)
        {
        }
    }
}
