﻿using iischef.core.Backup;
using iischef.core.SystemConfiguration;
using iischef.utils;
using Microsoft.Data.SqlClient;
using System;
using System.IO;
using System.Security.AccessControl;
using smo = Microsoft.SqlServer.Management.Smo;

namespace iischef.core.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class SQLService : DeployerBase, IDeployerInterface, IDeployerBackupInterface
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected SQLServer GetSqlServer(string serviceId)
        {
            // We can have ad-hoc settings for the SQL Connection in the application template itself
            if (this.Deployment.installedApplicationSettings.configuration["sqlservice_" + serviceId] != null)
            {
                this.Logger.LogInfo(true, "Using ad-hoc sql server settings set at the application level");
                SQLServer server = this.Deployment.installedApplicationSettings.configuration["sqlservice_" + serviceId].ToObject<SQLServer>();

                try
                {
                    // Instead of specyfing a real connection string, we can simply default to a global target
                    var targetServer = this.GlobalSettings.GetSqlServer(server.connectionString);
                    server.connectionString = targetServer.connectionString;
                }
                catch
                {
                    // ignored
                }

                return server;
            }

            // We can have an app_setting configuration
            // to route a whole application to a specific sql server
            string sqlTarget = null;

            if (this.Deployment.installedApplicationSettings.configuration["sqltarget"] != null)
            {
                sqlTarget = Convert.ToString(this.Deployment.installedApplicationSettings.configuration["sqltarget"]);
                this.Logger.LogInfo(true, "SQL Target overriden in local application settings to " + sqlTarget);
            }

            var sqlServer = this.GlobalSettings.GetSqlServer(sqlTarget);
            this.Logger.LogInfo(true, "SQL Target: " + sqlServer.id);

            return sqlServer;
        }

        /// <summary>
        /// 
        /// </summary>
        public void deploy()
        {
            var sqlSettings = this.DeployerSettings.castTo<SQLServiceSettings>();

            if (string.IsNullOrWhiteSpace(sqlSettings.id))
            {
                throw new Exception("SQL Service request id must have a value.");
            }

            // Figure out what SQLServer settings to use for this instance
            var sqlServer = this.GetSqlServer(sqlSettings.id);

            var id = this.Deployment.installedApplicationSettings.GetId() + "_" + sqlSettings.id;

            // Keys to store username, password and databasename
            string keylogin = $"services.{sqlSettings.id}.username";
            string keypassword = $"services.{sqlSettings.id}.password";
            string keydatabase = $"services.{sqlSettings.id}.database";

            // Parse the connection string
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(sqlServer.connectionString);

            // Make sure we can connect to the server
            var utils = new UtilsSqlServer(this.Logger);
            this.Logger.LogInfo(true, "Getting SQL connection '{0}'", sqlServer.connectionString);
            var connection = utils.GetServerConnection(sqlServer.connectionString);

            if (connection == null)
            {
                throw new Exception("Could not connect to the server: " + sqlServer.connectionString);
            }

            // The actual database name that will be used
            string databaseName;

            if (string.IsNullOrWhiteSpace(sqlServer.databaseName))
            {
                databaseName = "chf_" + id;
            }
            else
            {
                databaseName = sqlServer.databaseName;
            }

            this.Deployment.SetRuntimeSetting(keydatabase, databaseName);

            if (string.IsNullOrWhiteSpace(databaseName))
            {
                throw new Exception("Database name cannot be empty or null.");
            }

            // Ensure we have database and login.
            this.Logger.LogInfo(true, "Getting SQL database '{0}'", databaseName);
            var database = utils.FindDatabase(connection, databaseName, true);

            if (database == null)
            {
                throw new Exception("Could not  find database " + databaseName);
            }

            if (!database.Status.HasFlag(smo.DatabaseStatus.Normal))
            {
                throw new Exception("Database should be in 'Normal' status. The current database status is not compatible with automated deployments: " +
                                    database.Status);
            }

            // Recovery model can only be SIMPLE or FULL
            if (!string.IsNullOrWhiteSpace(sqlSettings.recoveryModel))
            {
                if (sqlSettings.recoveryModel == "SIMPLE" || sqlSettings.recoveryModel == "FULL")
                {
                    // Update the recovery mode
                    connection.ExecuteNonQuery(
                        $"ALTER DATABASE [{databaseName}] SET RECOVERY {sqlSettings.recoveryModel};");
                }
                else
                {
                    this.Logger.LogWarning(true, "Unrecognized SQL recovery model: {0}, use one of (SIMPLE, FULL)", sqlSettings.recoveryModel);
                }
            }

            // The database name, username and password must remain the same between deployments.
            // If we generated new user/pwd for new deployment, rollback functionality would NOT work as expected
            // as it would require re-deploying the logins.
            string dbLogin;
            string dbPassword;

            // If this is a passthrough authentication, propagate credentials as-is
            if (sqlServer.passThroughAuth)
            {
                dbLogin = builder.UserID;
                dbPassword = builder.Password;
            }
            else
            {
                dbLogin = "chf_" + id;
                dbPassword = this.Deployment.GetCredentials().Password;

                // This happens always, wether or not we have windows auth.
                this.Logger.LogInfo(true, "Adding SQL Login user '{0}' to database", dbLogin);
                smo.Login login = utils.EnsureLoginSql(connection, dbLogin, dbPassword, true);
                utils.BindUser(database, login, true);
            }

            this.Deployment.SetRuntimeSetting(keylogin, dbLogin);
            this.Deployment.SetRuntimeSetting(keypassword, dbPassword);

            // Create the database login, although we support windows auth,
            // the recommendation is to use SQL AUTH for portability reasons
            if (sqlServer.useWindowsAuth)
            {
                string sqlWindowsUserName = this.Deployment.GetCredentials().FormatUserNameForPrincipal(true);
                this.Logger.LogInfo(true, "Adding Windows Login user '{0}' to database", sqlWindowsUserName);

                // Depending on the setup this might fail, i.e. we are using a non-domain setup for chef (so the
                // application users are local and the server is in a domain).
                try
                {
                    smo.Login loginw = utils.EnsureLoginWindows(connection, sqlWindowsUserName, true);
                    utils.BindUser(database, loginw, true);
                }
                catch (Exception e)
                {
                    // 15401: "the domain controller for the domain where the login resides (the same or a different domain) is not available for some reason"
                    if ((e.InnerException?.InnerException as SqlException)?.Number != 15401)
                    {
                        throw;
                    }

                    this.Logger.LogError("Cannot add Windows login '{0}' to MSSQL Server '{1}'. This can happen if MSSQL and the local machine do not reside in the same domain.", sqlWindowsUserName, sqlServer.connectionString);
                }
            }

            this.Deployment.SetRuntimeSetting($"services.{sqlSettings.id}.host", builder.DataSource);

            // Build a connection string that the end user can handle
            SqlConnectionStringBuilder clientBuilder = new SqlConnectionStringBuilder();
            clientBuilder.UserID = dbLogin;
            clientBuilder.Password = dbPassword;
            clientBuilder.DataSource = builder.DataSource;
            clientBuilder.InitialCatalog = databaseName;
            this.Deployment.SetRuntimeSetting($"services.{sqlSettings.id}.connectionString", clientBuilder.ConnectionString);
            string preferredConnectionString = clientBuilder.ConnectionString;

            if (sqlServer.useWindowsAuth)
            {
                // Alternative connection string - integrated
                SqlConnectionStringBuilder clientBuilderWindowsAuth = new SqlConnectionStringBuilder();
                clientBuilderWindowsAuth.DataSource = builder.DataSource;
                clientBuilderWindowsAuth.IntegratedSecurity = true;
                clientBuilderWindowsAuth.InitialCatalog = databaseName;
                this.Deployment.SetRuntimeSetting($"services.{sqlSettings.id}.connectionStringWindowsAuth", clientBuilderWindowsAuth.ConnectionString);
                preferredConnectionString = clientBuilderWindowsAuth.ConnectionString;
            }

            this.Deployment.SetRuntimeSetting($"services.{sqlSettings.id}.connectionStringPreferred", preferredConnectionString);

            if (!string.IsNullOrWhiteSpace(sqlSettings.customScript))
            {
                using (var clientConnection = new SqlConnection(preferredConnectionString))
                {
                    clientConnection.Open();
                    var clientCommand = new SqlCommand(sqlSettings.customScript, clientConnection);
                    clientCommand.ExecuteNonQuery();
                }
            }
        }

        public void undeploy(UndeployMode mode)
        {
            if (mode == UndeployMode.Undeploy)
            {
                return;
            }

            var sqlSettings = this.DeployerSettings.castTo<SQLServiceSettings>();
            var sqlServer = this.GetSqlServer(sqlSettings.id);

            string keylogin = $"services.{sqlSettings.id}.username";
            string keydatabase = $"services.{sqlSettings.id}.database";

            // The database name, username and password must remain the same between deployments.
            // If we generated new user/pwd for new deployment, rollback functionality would NOT work as expected
            // as it would require re-deploying the logins.
            var dbLogin = this.Deployment.GetRuntimeSetting(keylogin, null);
            var dbDatabase = this.Deployment.GetRuntimeSetting(keydatabase, null);

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(sqlServer.connectionString);

            var utils = new UtilsSqlServer(this.Logger);

            var connection = utils.GetServerConnection(sqlServer.connectionString);

            if (connection != null)
            {
                // Only remove if the database was autogenerated
                if (sqlServer.databaseName.IsNullOrDefault())
                {
                    if (mode == UndeployMode.UninstallAndCleanup)
                    {
                        utils.DeleteDatabase(connection, dbDatabase);
                    }
                    else if (mode == UndeployMode.Uninstall)
                    {
                        this.Logger.LogWarning(false, "Orphan database needs to be manually removed: " + dbDatabase);
                    }
                }

                // Only remove if the login is not the same as the one in the master connection
                if (builder.UserID != dbLogin)
                {
                    utils.DeleteLogin(connection, dbLogin);
                }

                utils.DeleteLogin(connection, this.Deployment.GetCredentials().FormatUserNameForPrincipal(true));
            }
        }

        public void start()
        {
        }

        public void stop()
        {
        }

        public void shutdown()
        {
        }

        public void deploySettings(
            string jsonSettings,
            string jsonSettingsNested,
            RuntimeSettingsReplacer replacer)
        {
        }

        /// <inheritdoc cref="IDeployerBackupInterface"/>
        public void Backup(ApplicationBackupSettings backupSettings)
        {
            var sqlSettings = this.DeployerSettings.castTo<SQLServiceSettings>();
            var sqlServer = this.GetSqlServer(sqlSettings.id);

            string keydatabase = $"services.{sqlSettings.id}.database";

            // The database name, username and password must remain the same between deployments.
            // If we generated new user/pwd for new deployment, rollback functionality would NOT work as expected
            // as it would require re-deploying the logins.
            var dbDatabase = this.Deployment.GetRuntimeSetting(keydatabase, null);

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(sqlServer.connectionString);

            var utils = new UtilsSqlServer(this.Logger);

            var connection = utils.GetServerConnection(sqlServer.connectionString);

            if (connection != null)
            {
                this.Logger.LogInfo(true, $"Backup up catalog {dbDatabase} from {builder.DataSource}.");

                string targetPath = Path.Combine(backupSettings.TargetDirectory, "mssql", sqlSettings.id + ".bak");
                UtilsSystem.EnsureDirectoryExists(targetPath);
                UtilsWindowsAccounts.AddSsidPermissionToDir(Path.GetDirectoryName(targetPath), UtilsWindowsAccounts.WELL_KNOWN_SID_EVERYONE, FileSystemRights.FullControl);

                var command = $"BACKUP DATABASE [{dbDatabase}] TO DISK='{targetPath}' WITH COPY_ONLY, COMPRESSION";
                connection.ExecuteNonQuery(command);
            }
        }
    }
}
