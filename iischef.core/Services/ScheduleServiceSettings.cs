﻿using System.Collections.Generic;

namespace iischef.core.Services
{
    public class ScheduleServiceSettings : DeployerSettingsBase
    {
        /// <summary>
        /// 
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Use for a single command. Only here for BC reasons.
        /// </summary>
        public string command { get; set; }

        /// <summary>
        /// Use for multiple commands
        /// </summary>
        public List<string> commands { get; set; }

        /// <summary>
        /// Execution frequency, in minutes.
        /// </summary>
        public int frequency { get; set; }

        /// <summary>
        /// Frecuency in crontab format
        /// </summary>
        public string crontab { get; set; }

        /// <summary>
        /// The user ID used for the task. Use "auto" to configure
        /// the application specific user.
        /// </summary>
        public string taskUserId { get; set; }

        /// <summary>
        /// The password for the user
        /// </summary>
        public string taskUserPassword { get; set; }

        /// <summary>
        /// The logon type for the task.
        /// </summary>
        public int? taskLogonType { get; set; }

        /// <summary>
        /// Set to true to install this as a disabled cron job
        /// </summary>
        public bool disabled { get; set; }

        /// <summary>
        /// Exeuction time limit in minutes
        /// </summary>
        public int? executionTimeLimitMinutes { get; set; }

        /// <summary>
        /// AllowHardTerminate
        /// </summary>
        public bool? allowHardTerminate { get; set; }

        /// <summary>
        /// Force stop the task at end of repetition duration
        /// </summary>
        public bool? stopAtEndOfRepetitionDuration { get; set; }
    }
}
