﻿using iischef.core.Configuration;
using iischef.logger;
using iischef.utils;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace iischef.core
{
    public class DeployerBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected SystemConfiguration.EnvironmentSettings GlobalSettings;

        /// <summary>
        /// 
        /// </summary>
        protected Deployment Deployment;

        /// <summary>
        /// 
        /// </summary>
        protected JObject DeployerSettings;

        /// <summary>
        /// 
        /// </summary>
        protected ILoggerInterface Logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalSettings"></param>
        /// <param name="deployerSettings"></param>
        /// <param name="deployment"></param>
        /// <param name="logger"></param>
        /// <param name="inhertApp"></param>
        public virtual void initialize(
                SystemConfiguration.EnvironmentSettings globalSettings,
                JObject deployerSettings,
                Deployment deployment,
                ILoggerInterface logger)
        {
            this.Deployment = deployment;
            this.GlobalSettings = globalSettings;
            this.DeployerSettings = deployerSettings;
            this.Logger = logger;
        }

        public int weight { get; set; }

        public void deployConsoleEnvironment(StringBuilder command)
        {
        }

        /// <inheritdoc cref="IDeployerInterface"/>
        public virtual void done()
        {
        }

        /// <inheritdoc cref="IDeployerInterface"/>
        public virtual void beforeDone()
        {
        }

        /// <inheritdoc cref="IDeployerInterface"/>
        public virtual void cleanup()
        {
        }

        /// <inheritdoc cref="IDeployerInterface"/>
        public virtual void cron()
        {
        }
    }
}
