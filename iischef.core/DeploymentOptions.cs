﻿namespace iischef.core
{
    /// <summary>
    /// 
    /// </summary>
    public static class DeploymentOptions
    {
        /// <summary>
        /// Certificates are self-signed always
        /// </summary>
        public const string SELF_SIGN_CERTIFICATES = "self-sign-certificates";

        /// <summary>
        /// All bindings are pushed to the localhost/127.0.0.1 network interface
        /// </summary>
        public const string IIS_FORCE_ALL_LOCALHOST = "iis-force-all-local";

        /// <summary>
        /// Disable all scheduled tasks
        /// </summary>
        public const string SCHEDULER_DISABLE = "scheduler-disabled";
    }
}
