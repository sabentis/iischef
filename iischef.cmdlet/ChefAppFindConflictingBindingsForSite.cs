﻿using iischef.utils;
using System.Linq;
using System.Management.Automation;

namespace iischef.cmdlet
{
    /// <summary>
    /// Busca los bindings conflictivos de este site que podrían colisionar con otros sites
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppFindConflictingBindingsForSite")]
    public class ChefAppFindConflictingBindingsForSite : Cmdlet
    {
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true)]
        public string SiteName { get; set; }

        protected override void ProcessRecord()
        {
            var logger = new logger.ConsoleLogger();
            logger.SetVerbose(true);

            var duplicates = UtilsIis.ParseSiteBindings(logger);

            // Buscamos todos los bindings de ese sitio
            var bindings = duplicates
                .Where((i) => i.Sites.Contains(this.SiteName));

            foreach (var duplicate in bindings)
            {
                var sites = duplicate.Sites;

                // Las parejas de sitios con su offline no nos interesan
                foreach (var site in sites.ToList())
                {
                    if (sites.Contains("off_" + site))
                    {
                        sites.Remove("off_" + site);
                    }
                }

                if (!sites.Any())
                {
                    continue;
                }

                logger.LogInfo(true, $"{duplicate.Binding.Hostname}::{duplicate.Binding.Port} ({string.Join(", ", sites)})");
            }
        }
    }
}
