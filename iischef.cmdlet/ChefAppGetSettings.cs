﻿using iischef.core;
using iischef.core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using Console = System.Console;

namespace iischef.cmdlet
{
    /// <summary>
    /// Shows the current application settings
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppGetSettings")]
    [OutputType(typeof(List<InstalledApplication>))]
    public class ChefAppGetSettings : Cmdlet
    {
        [Parameter(Position = 1, Mandatory = true)]
        public string Id { get; set; }

        [Alias("o")]
        [Parameter(HelpMessage = "If true, open a notepad.exe, you can specify the application with OpenWith parameter")]
        public SwitchParameter Open { get; set; }

        [Alias("ow")]
        [Parameter]
        public string OpenWith { get; set; } = "notepad.exe";

        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.GetApplicationForConsole();

            var installedApplication = app.GetInstalledApp(this.Id);

            if (installedApplication == null)
            {
                throw new Exception("Application not found: " + this.Id);
            }
            
            var filePath = installedApplication.GetActiveSettingsPath(app.GetGlobalSettings());

            if (this.Open.IsPresent)
            {
                var logger = app.GetLogger();
                logger.LogInfo(true, $"Opening file with {this.OpenWith}"); 
                System.Diagnostics.Process.Start(this.OpenWith, filePath);
                return;
            }
            
            Console.WriteLine("Installed application Chef Settings");
            Console.WriteLine(System.IO.File.ReadAllText(filePath));
        }
    }
}
