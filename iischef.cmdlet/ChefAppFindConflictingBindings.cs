﻿using iischef.utils;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;

namespace iischef.cmdlet
{
    /// <summary>
    /// Analiza toda la configuración del IIS y busca bindings conflictivos
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppFindConflictingBindings")]
    public class ChefAppFindConflictingBindings : Cmdlet
    {
        protected override void ProcessRecord()
        {
            var logger = new logger.ConsoleLogger();
            logger.SetVerbose(true);

            var duplicates = UtilsIis.ParseSiteBindings(logger)
                .Where((i) => i.Sites.Count > 1)
                .OrderBy((i) => i.Binding.Hostname)
                .ToList();

            foreach (var duplicate in duplicates)
            {
                logger.LogInfo(true, $"{duplicate.Binding.Hostname}::{duplicate.Binding.Port} ({string.Join(", ", duplicate.Sites)})");
            }
        }
    }
}
