﻿using iischef.core;
using System.Management.Automation;

namespace iischef.cmdlet
{
    /// <summary>
    /// Re-Depoy an application using it's ID, or redeploy ALL app's!
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppBackup")]
    [OutputType(typeof(Deployment))]
    public class ChefAppBackup : Cmdlet
    {
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true)]
        public string Id { get; set; }

        [Parameter(Position = 2, ValueFromPipelineByPropertyName = true)]
        public string BackupPath { get; set; }

        [Parameter(Position = 3, ValueFromPipelineByPropertyName = true)]
        public string ChangedSince { get; set; }

        protected override void ProcessRecord()
        {
            ConsoleUtils.RunCode(() =>
            {
                var app = ConsoleUtils.GetApplicationForConsole();
                app.BackupAppById(this.Id, this.BackupPath, this.ChangedSince);
            });
        }
    }
}
