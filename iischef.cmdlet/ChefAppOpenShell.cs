﻿using iischef.core;
using System;
using System.Diagnostics;
using System.IO;
using System.Management.Automation;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace iischef.cmdlet
{
    /// <summary>
    /// Open a console for an application
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppOpenShell")]
    [OutputType(typeof(Deployment))]
    public class ChefAppOpenShell : PSCmdlet
    {
        [Parameter(ValueFromPipelineByPropertyName = true, Mandatory = true)]
        public string Id { get; set; }

        protected override void ProcessRecord()
        {
            ConsoleUtils.RunCode(() =>
            {
                var app = ConsoleUtils.GetApplicationForConsole();

                var installedApp = app.GetInstalledApp(this.Id);
                var deployer = app.GetDeployer(installedApp);

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "powershell.exe";

                var path = Path.Combine(deployer.DeploymentActive.runtimePath, "chef-settings-nested.json");
                startInfo.Arguments = "-NoExit -Command \"$Env:CHEF_CONFIGNESTED= '" + path + "';\"";
                startInfo.WorkingDirectory = deployer.DeploymentActive.appPath;
                startInfo.WindowStyle = ProcessWindowStyle.Normal;
                startInfo.UseShellExecute = false;

                startInfo.UserName = deployer.DeploymentActive.windowsUsername;
                startInfo.PasswordInClearText = deployer.DeploymentActive.GetCredentials().Password;
                startInfo.Domain = deployer.DeploymentActive.globalSettings.directoryPrincipal?.DomainName;
                startInfo.LoadUserProfile = true;

                // **************************
                // No usar esto para poner variables de entorno, usar lo de más arriba (Argumentos)
                // **************************

                ////var homeDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), deployer.DeploymentActive.windowsUsername);
                ////startInfo.EnvironmentVariables["UserProfile"] = homeDirectory;
                // startInfo.EnvironmentVariables["CHEF_CONFIGNESTED"] = path;
                ////string consoleHostHistoryPath = Path.Combine(homeDirectory, "Documents", "WindowsPowerShell", "PSReadLine", "ConsoleHost_history.txt");
                ////startInfo.EnvironmentVariables["ConsoleHostHistoryPath"] = consoleHostHistoryPath;

                Process p = new Process();
                p.StartInfo = startInfo;
                p.Start();

                int timeout = 5000; // Timeout in milliseconds
                Stopwatch sw = Stopwatch.StartNew();

                var title = "Chef: " + deployer.DeploymentActive.installedApplicationSettings.GetId();

                // Wait for the process to start and its window to become available
                while (GetWindowText(p.MainWindowHandle) != title && sw.ElapsedMilliseconds < timeout)
                {
                    SetWindowText(p.MainWindowHandle, title);
                    Thread.Sleep(250);
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        private static extern IntPtr SetWindowText(IntPtr hWnd, string text);

        // Declare the GetWindowText function using platform invoke
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        // Helper method to get the window text of a given window handle
        private static string GetWindowText(IntPtr hWnd)
        {
            const int nChars = 256;
            StringBuilder sb = new StringBuilder(nChars);

            if (GetWindowText(hWnd, sb, nChars) > 0)
            {
                return sb.ToString();
            }

            return string.Empty;
        }
    }
}
