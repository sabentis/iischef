﻿using iischef.core;
using iischef.core.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Automation;
using Console = System.Console;

namespace iischef.cmdlet
{
    /// <summary>
    /// Shows the application template in installed apps template directory
    /// </summary>
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppGetTemplate")]
    [OutputType(typeof(List<InstalledApplication>))]
    public class ChefAppGetTemplate : Cmdlet
    {
        [Parameter(Position = 1, Mandatory = true)]
        public string Id { get; set; }

        [Alias("o")]
        [Parameter]
        public SwitchParameter Open { get; set; }

        [Alias("ow")]
        [Parameter]
        public string OpenWith { get; set; } = "notepad.exe";

        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.GetApplicationForConsole();

            var installedAppsFolder = app.GetGlobalSettings().applicationTemplateDir;
            var logger = app.GetLogger();

            if (installedAppsFolder == null)
            {
                logger.LogError("There's not application template directory");
            }

            var difo = new DirectoryInfo(installedAppsFolder);

            string templateFile = null;

            foreach (var file in difo.EnumerateFiles()
    .Where((i) => i.Extension?.Equals(".json", StringComparison.InvariantCultureIgnoreCase) == true
                  || i.Extension?.Equals(".yaml", StringComparison.InvariantCultureIgnoreCase) == true
                  || i.Extension?.Equals(".yml", StringComparison.InvariantCultureIgnoreCase) == true))
            {
                var installedApplication = new InstalledApplication();
                installedApplication.ParseFromFile(file.FullName);
                
                if (installedApplication.GetId() == this.Id)
                {
                    templateFile = file.FullName;
                    break;
                }
            }

            if (string.IsNullOrWhiteSpace(templateFile))
            {
                logger.LogError("There's not application template file");
                return;
            }

            if (this.Open.IsPresent)
            {
                logger.LogInfo(true, $"Opening file with {this.OpenWith}");
                System.Diagnostics.Process.Start(this.OpenWith, templateFile);
                return;
            }

            Console.WriteLine(System.IO.File.ReadAllText(templateFile));
        }
    }
}
