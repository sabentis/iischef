﻿using iischef.core;
using iischef.utils;
using System;
using System.Management.Automation;

namespace iischef.cmdlet
{
    [Cmdlet(VerbsLifecycle.Invoke, "ChefTestInputOrWait")]
    public class ChefAppTestInputOrWait : Cmdlet
    {
        protected override void ProcessRecord()
        {
            ConsoleUtils.RunCode(() =>
            {
                var app = ConsoleUtils.GetApplicationForConsole();
                app.GetLogger().LogWarning(true, "Wait for 10 seconds or press a key to continue...");

                string input = null;

                try
                {
                    input = Reader.ReadLine(10000);
                }
                catch (TimeoutException)
                {
                }

                app.GetLogger().LogInfo(true, "Input is: " + input);
            });
        }
    }
}
