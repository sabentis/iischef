﻿using iischef.core;
using iischef.core.Configuration;
using System.Collections.Generic;
using System.Management.Automation;

namespace iischef.cmdlet
{
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppUninstall", SupportsShouldProcess = true)]
    [OutputType(typeof(List<InstalledApplication>))]
    public class ChefAppUninstall : Cmdlet
    {
        [Parameter(Position = 1, ValueFromPipelineByPropertyName = true, Mandatory = true)]
        public string Id { get; set; }

        [Parameter]
        public SwitchParameter Force { get; set; }

        [Parameter]
        public SwitchParameter Delete { get; set; }

        protected override void ProcessRecord()
        {
            bool yesToAll = false;
            bool noToAll = false;

            if (this.Delete.IsPresent)
            {
                if (this.ShouldContinue(
                    "This will uninstall transient and persitent resources. All data will be erased.",
                    "Uninistall and delete application", 
                    true, 
                    ref yesToAll,
                    ref noToAll))
                {
                    ConsoleUtils.RunCode(() =>
                    {
                        var app = ConsoleUtils.GetApplicationForConsole();
                        app.RemoveAppById(this.Id, this.Force, UndeployMode.UninstallAndCleanup);
                    });
                }
            }
            else
            {
                if (this.ShouldContinue(
                    "This will uninstall transient resources, but persistent resources must be deleted manually. Use the -Delete flag to automatically delete persistent contents.",
                    "Uninistall application", 
                    true, 
                    ref yesToAll, 
                    ref noToAll))
                {
                    ConsoleUtils.RunCode(() =>
                    {
                        var app = ConsoleUtils.GetApplicationForConsole();
                        app.RemoveAppById(this.Id, this.Force, UndeployMode.Uninstall);
                    });
                }
            }
        }
    }
}
