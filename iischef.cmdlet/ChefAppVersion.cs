﻿using iischef.core;
using iischef.core.Configuration;
using System.Collections.Generic;
using System.Management.Automation;
using Console = System.Console;

namespace iischef.cmdlet
{
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppVersion")]
    [OutputType(typeof(List<InstalledApplication>))]
    public class ChefAppVersion : Cmdlet
    {
        protected override void ProcessRecord()
        {
            var app = ConsoleUtils.GetApplicationForConsole();
            Console.WriteLine("Version: " + app.GetGlobalSettings().BuildBranch);
        }
    }
}
