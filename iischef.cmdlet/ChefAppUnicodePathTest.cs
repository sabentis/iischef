﻿using iischef.core;
using iischef.utils;
using System.Management.Automation;

namespace iischef.cmdlet
{
    [Cmdlet(VerbsLifecycle.Invoke, "ChefAppUnicodePathTest", SupportsShouldProcess = true)]
    [OutputType(typeof(void))]
    public class ChefAppUnicodePathTest : Cmdlet
    {
        protected override void ProcessRecord()
        {
            ConsoleUtils.RunCode(() =>
            {
                var logger = new logger.ConsoleLogger();
                logger.LogInfo(false, "Unicode support:" + UtilsSystem.FileSystemSupportsUnicodeFileNames);
                UtilsSystem.DoTestUnicodePaths();
            });
        }
    }
}
